const functions = require("firebase-functions");
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

const express = require("express");
const app = express();
const stripe = require("stripe")(`sk_test_51KWeT4AQjYGFyGKWsGvy3viKa49wPKLg3QhQ2SVsO1Lwu6yqNlouWlRIPMnmZ4Zh0iSpM0FsbVU071lYTI422EIO00BkWW3A3E`);


exports.createStripeAccount = functions.https.onCall((data) => {

  try {
    const email = data.email;
    const country = data.country;
    const businessType = data.business_type;
    const account = stripe.accounts.create({
      type: "express",
      email: email,
      country: country,
      capabilities: {
        card_payments: { requested: true },
        transfers: { requested: true },
      },
      business_type: businessType,

    });
    if (account) {
      const accountLink = stripe.accountLinks.create({
        account: account.id,
        refresh_url: "https://example.com/reauth",
        return_url: "https://example.com/return",
        type: "account_onboarding",
      });
      if (accountLink) {
        return "success : true";
        console.log("KYC LINK : ", accountLink);
      } else {
        console.log("Email are not sending");
        return "success : false";
      }
    } else {
      return "success : false";
    }
    
  } catch (err) {
    return "success : false";
  }
});